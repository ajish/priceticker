

function getLastTick( compound_key ){
    var comps = compound_key.split(",");
    pub_id = comps[0];
    domain = comps[1];
    geo = comps[2];
    var result;
    jQuery.ajax({
        url: "metrics?publisher_id="+pub_id+"&domain="+domain+"&geo_country="+geo,
        async: false,
        success: function(data){
           result = data;
        }
     });   
    return result;
}
	
function plotDynamicChart( container, compound_key, last_price ){
    Highcharts.setOptions({
		global : {
			useUTC : false
		}
	});
	
    return new Highcharts.StockChart({
		chart : {
			renderTo : container,
			events : {
				load : function() {
					var series = this.series[0];
					setInterval(function() {
						var x = (new Date()).getTime(),
						y = parseFloat(getLastTick(compound_key)["last_price"]);
						series.addPoint([x, y], true, true);
					}, 1000);
				}
			}
		},
		
		rangeSelector: {
			buttons: [{
				count: 1,
				type: 'minute',
				text: '1M'
			}, {
				count: 5,
				type: 'minute',
				text: '5M'
			}, {
				type: 'all',
				text: 'All'
			}],
			inputEnabled: false,
			selected: 0
		},
		
		title : {
			text : compound_key 
		},

        legend: {
          enabled: false
        },

        exporting: {
          enabled: false
        },

        series: [{
          name: 'Last Price',
          data: (function() {
              var data = [],
                  time = (new Date()).getTime(),
                  i;
              for (i = -19; i <= 0; i++) {
                  data.push({
                                x: time + i * 1000,
                                y: last_price
                            });
              }
              return data;
          })()

        }]
	});
}
