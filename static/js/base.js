function get_more_prices(){
  $.ajax({
    url: "/get_more_prices",
    success: get_more_prices_callback
  });
}

function get_more_prices_callback (results) {

  var stocks = '';
  for(i = 0; i < 10; i++) {
    stocks += ' ' + results[i]['ticker'] + ' - ' + results[i]['price'] + ' / ';

    $('#name_' + i).hide().text(results[i]['ticker']).fadeIn(10);
    $('#price_' + i).hide().text(results[i]['price']).fadeIn(10);
    $('#volume_' + i).hide().text(results[i]['volume']).fadeIn(10);

    direction = results[i]['direction'];
    $('#direction_' + i).hide().text('').fadeIn(10);
    if(direction == 'Up Arrow'){
      $('#direction_' + i).attr('class','up');
      $('#direction_' + i).append("&nbsp;<img src='images/green.gif' id='up_arrow' />");
    }else{
      $('#direction_' + i).attr('class','down');
      $('#direction_' + i).append("&nbsp;<img src='images/red.gif' id='up_arrow' />");
    }

  }

  $('#ticker_marquee').text($('#ticker_marquee').text() + stocks);

  for(i = 0; i < 10; i++) {
    $('#p_name_' + i).hide().text(results[i]['ticker']).fadeIn(10);
    $('#p_price_' + i).hide().text(results[i]['price']).fadeIn(10);
    $('#p_volume_' + i).hide().text(results[i]['volume']).fadeIn(10);

    direction = results[i]['direction'];
    $('#p_direction_' + i).hide().text('').fadeIn(10);
    if(direction == 'Up Arrow'){
      $('#p_direction_' + i).attr('class','up');
      $('#p_direction_' + i).append("&nbsp;<img src='images/green.gif' id='up_arrow' />");
    }else{
      $('#p_direction_' + i).attr('class','down');
      $('#p_direction_' + i).append("&nbsp;<img src='images/red.gif' id='up_arrow' />");
    }

  }

  setTimeout("get_more_prices()", 3000);
}

var lastActiveTab = "volume_tab_header";

function switch_tab(tab_name){
  $('#volume_tab').hide();
  $('#price_tab').hide();
  $('#' + tab_name).fadeIn(1500);
  $('#' + lastActiveTab).removeClass("active");
  lastActiveTab = tab_name + "_header";
  $('#' + lastActiveTab).addClass("active");
}
