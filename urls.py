from django.conf import settings
from django.conf.urls.defaults import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',

    # Pages
    ('^$', 'appathon.app.views.render'),

    # APIs
    ('^search$', 'appathon.app.views.search'),
    ('^get_more_prices$', 'appathon.app.views.get_more_prices'),
    ('^metrics', 'appathon.app.views.metrics'),
    ('^tickers', 'appathon.app.views.tickers'),
)

# Add Static References
urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^(?P<path>.*)$', 'django.views.static.serve',
         {'document_root': settings.STATIC_DOC_ROOT}),
    )
