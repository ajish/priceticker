import sys
from pandas.io.pytables import HDFStore
import pandas
from sucker import TickerStore
import ticker_config
import datetime
import numpy as np

feeds_f = pandas.io.parsers.read_csv(ticker_config.SYMBOL_FILE)
feeds = feeds_f[TickerStore.KEYS]

d = datetime.datetime(2012,03,16)
delta = datetime.timedelta(seconds=1)
enddate = d + datetime.timedelta(minutes=30)

a = HDFStore("blah.h5")

objs = feeds.shape[0]
try:
    f = a['store']
except:
    f = None

while d <= enddate:
    print d.strftime("%Y-%m-%d %H:%M:%S")
    d += delta
    mat = feeds
    mat['datetime']=d
    mat['clear_price'] = np.random.random(objs)
    mat['volume'] = np.random.random_integers( 10,1000, objs)
    if not f:
        a['store'] = mat
    else:
        a['store'] = a['store'].append( mat, ignore_index=True)
    

