from os.path import dirname, join
from csv import reader, DictReader
from django.conf import settings

def venues():
    path = join(dirname(__file__), 'data', settings.VENUE_FILE)
    csvfile = open(path, 'rb')
    header = reader(csvfile).next()
    return [row for row in DictReader(csvfile, header)]