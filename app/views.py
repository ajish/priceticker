# Python
from datetime import datetime
import random
from simplejson import dumps

# Django
from django.shortcuts import render_to_response
from django.http import HttpResponse

from querystring_parser import parser

from utils import venues
from lib.leech.sucker import ticker_store

def json_view(func):
    def wrap(request, *a, **kw):
        response = func(request, *a, **kw)
        assert isinstance(response, dict)
        if 'result' not in response:
            response['result'] = 'ok'
        json = dumps(response)
        return HttpResponse(json, mimetype='application/json')
    return wrap

@json_view
def metrics(request):
    publisher_id = request.GET.get('publisher_id', '')
    geo_country = request.GET.get('geo_country', '')
    domain = request.GET.get('domain', '')
    if publisher_id and geo_country and domain:
        key = (int(publisher_id), domain, geo_country)
        return ticker_store.get_metrics_for_key(key)

@json_view
def tickers(request):
    return {"tickers": ticker_store.get_metrics_for_all_keys()}

def get_more_prices(request):

    results_dict = {}

    rank = 0
    ticker_data = ticker_store.get_top10_byVolume()
    for label, row in ticker_data.iteritems():
        ticker_tuple = label
        volume = row
        metrics = ticker_store.get_metrics_for_key(ticker_tuple)
        ticker = '%s (%s)' % (ticker_tuple[1], ticker_tuple[2])
        
        results_dict[str(rank)] = {}
        results_dict[str(rank)]['ticker'] = ticker
        if volume > 10000:
            volume = 13 * random.randint(1,10000)
        results_dict[str(rank)]['volume'] = format(int(volume), ",d")
        results_dict[str(rank)]['price'] = '${:.2f}'.format(metrics['last_price'])

        if random.randint(1,10) > 5:
            results_dict[str(rank)]['direction'] = 'Up Arrow'
        else:
            results_dict[str(rank)]['direction'] = 'Down Arrow'
        rank += 1

    return HttpResponse(dumps(results_dict), mimetype='application/json')
            
@json_view
def search(request):
    query = request.GET.get("query", "No Results")

    ticker = query
    price = 4.1
    volume = 500
    last_update = str(datetime.now().strftime("%I:%M %p"))
    high_price = 5.00
    low_price = 4.00

    results = {"ticker":ticker,
               "price":price,
               "volume":volume,
               "last_update":last_update,
               "high_price":high_price,
               "low_price":low_price
               }
    return results

def render(request):
    main_page = MainPage(request)
    return main_page.render()

class MainPage(object):

    tickers = None

    def __init__(self, request):
        parsed = parser.parse(request.META['QUERY_STRING'])
        self.js_api_url = parsed.get('js_api_url', '')
        self.config_object = dumps(parsed.get('config_object', ''))
        if self.tickers is None:
            self.tickers = venues()

    def render(self):
        return render_to_response('index.html',
                                  dict(js_api_url=self.js_api_url,
                                       config_object=self.config_object,
                                       stocks=range(10),
                                       ticker_list=self.tickers))
