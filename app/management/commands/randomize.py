from django.core.management.base import BaseCommand
from app.utils import venues
from random import choice, random, randint
from redis import Redis
from time import sleep
from sys import stdout

class Command(BaseCommand):
    def handle(self, *args, **options):
        redis = Redis('optimization.dev.adnxs.net')
        all_venues = venues()
        count = 0
        while True:
            chosen = choice(all_venues)
            redis.publish('ANX', '%s,%.2f' % (','.join([chosen['publisher_id'],
                                                        chosen['url'],
                                                        chosen['geo_country']]),
                                              randint(1, 5) * random()))
            stdout.write('.')
            stdout.flush()
            count = (count + 1) % 10
            if not count:
                sleep(1)
